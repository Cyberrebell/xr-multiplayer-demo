extends CollisionShape3D

@onready var character : CharacterBody3D = get_parent()
@onready var head : XRCamera3D = character.find_child("XRCamera3D")
@onready var hand_left : OpenXRHand = character.find_child("hand_left")
@onready var hand_right : OpenXRHand = character.find_child("hand_right")

func _process(_delta) -> void:
	if character.is_multiplayer_authority():
		push_movement()
		push_body()
	else:
		pull_movement()
		pull_body()

func pull_movement() -> void:
	character.position = get_meta("sync_position")
	character.rotation = get_meta("sync_rotation")
	character.velocity = get_meta("sync_velocity")

func push_movement() -> void:
	set_meta("sync_position", character.position)
	set_meta("sync_rotation", character.rotation)
	set_meta("sync_velocity", character.velocity)

func pull_body() -> void:
#	head.position = get_meta("sync_head_position")
#	head.global_rotation = get_meta("sync_head_rotation")
	hand_left.position = get_meta("sync_hand_left_position")
	hand_left.global_rotation = get_meta("sync_hand_left_rotation")
	hand_right.position = get_meta("sync_hand_right_position")
	hand_right.global_rotation = get_meta("sync_hand_right_rotation")

func push_body() -> void:
	set_meta("sync_head_position", character.to_local(head.global_position))
	set_meta("sync_head_rotation", head.global_rotation)
	set_meta("sync_hand_left_position", character.to_local(hand_left.global_position))
	set_meta("sync_hand_left_rotation", hand_left.global_rotation)
	set_meta("sync_hand_right_position", character.to_local(hand_right.global_position))
	set_meta("sync_hand_right_rotation", hand_right.global_rotation)
