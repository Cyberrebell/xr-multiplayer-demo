extends Node

class_name TorsoLift

@onready var torso : Node3D = get_parent()
var locator : Node3D
@onready var torso_default_height : float = torso.position.y
var locator_height : float = 1.6

func _ready():
	locator = BodyRelocator.find_body_relocator(torso.get_parent()).get_parent()

func _process(_delta):
	update()

func calibrate():
	locator_height = locator.position.y

func update() -> void:
	torso.position.y = torso_default_height - (locator_height - locator.position.y)
