extends Node

class_name BodyRelocator

@onready var locator : Node3D = get_parent()
@onready var xr_origin : XROrigin3D = BodyRelocator.find_xr_origin(locator)
@onready var character_body : CharacterBody3D = BodyRelocator.find_character_body(xr_origin)
var current_body : CharacterBody3D
var restrained_xr_origin_movement : Vector3 = Vector3.ZERO

signal character_body_updated
signal rotation_reset

var last_position : Vector2 = Vector2(0, 0)
var seat
var seat_in_range
signal climb_stopped

static func find_xr_origin(base : Node) -> XROrigin3D:
	while !base is XROrigin3D:
		base = base.get_parent()
	return base

static func find_character_body(base : Node) -> CharacterBody3D:
	while !base is CharacterBody3D:
		base = base.get_parent()
	return base

static func find_body_relocator(parent : Node) -> BodyRelocator:
	return parent.find_children("", "BodyRelocator").front()

func _ready():
	unfreeze()
	climb_stopped.connect(sit_down)

func _process(_delta):
	var movement = Vector3(0, 0, 0)
	movement.x = locator.position.x - last_position.x
	movement.z = locator.position.z - last_position.y
	if weakref(seat).get_ref() is XRCharacterSeat and seat.is_in_head_motion_range(locator.global_position + movement):
		restrained_xr_origin_movement += movement
	else:
		xr_origin.position -= movement
	movement = movement.rotated(Vector3.UP, xr_origin.global_rotation.y)
	current_body.position += movement
	last_position.x = locator.position.x
	last_position.y = locator.position.z
	if weakref(seat).get_ref() is XRCharacterSeat:
		seat.relocate(character_body)

func freeze() -> void:
	current_body = CharacterBody3D.new()
	character_body.add_child(current_body)
	character_body_updated.emit(current_body)

func unfreeze() -> void:
	if current_body is CharacterBody3D and current_body != character_body:
		character_body.remove_child(current_body)
	current_body = character_body
	character_body_updated.emit(current_body)

func stand_up():
	if weakref(seat).get_ref() is XRCharacterSeat:
		seat.character_stand_up.emit(character_body)
		seat = null
		unfreeze()
		character_body.rotation.x = 0
		character_body.rotation.z = 0
		xr_origin.position -= restrained_xr_origin_movement
		restrained_xr_origin_movement = Vector3.ZERO
		rotation_reset.emit()

func sit_down():
	if seat_in_range is XRCharacterSeat and seat_in_range.is_in_head_target(locator.global_position):
		seat = seat_in_range
		freeze()
		seat.character_sit_down.emit(character_body)
