extends Node

class_name BodyDirector

@onready var character_body : CharacterBody3D = BodyRelocator.find_character_body(get_parent())
var body_relocator : BodyRelocator

func init_relocator():
	body_relocator = BodyRelocator.find_body_relocator(character_body)
	body_relocator.character_body_updated.connect(update_body)
	body_relocator.rotation_reset.connect(reset_rotation)

func update_body(body : CharacterBody3D) -> void:
	character_body = body
	if character_body.get_children().is_empty():
		xr_pivot = character_body
	else:
		xr_pivot = BodyRelocator.find_xr_origin(director).get_parent()

@onready var director : Node3D = get_parent()
@onready var xr_pivot : Node3D = BodyRelocator.find_xr_origin(director).get_parent()
var last_rotation : float = 0

func get_rotation() -> float:
	return -director.rotation.y

func _ready():
	init_relocator()

func _process(_delta):
	var rotation = director.rotation.y - last_rotation
	last_rotation = director.rotation.y
	character_body.rotate_y(rotation)
	xr_pivot.rotate_y(-rotation)

func reset_rotation() -> void:
	xr_pivot.rotation.y = get_rotation()
	character_body.rotation.y = director.rotation.y
