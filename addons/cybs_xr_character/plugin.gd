@tool
extends EditorPlugin

func _enter_tree():
	add_custom_type("BodyDirector", "Node", preload("body/BodyDirector.gd"), preload("icon.png"))
	add_custom_type("BodyRelocator", "Node", preload("body/BodyRelocator.gd"), preload("icon.png"))
	add_custom_type("HandObserver", "Node", preload("input/HandObserver.gd"), preload("icon.png"))
	add_custom_type("JumpButton", "AbstractControllerInput", preload("input/JumpButton.gd"), preload("icon.png"))
	add_custom_type("ThumbstickLocomotion", "AbstractControllerInput", preload("input/ThumbstickLocomotion.gd"), preload("icon.png"))
	add_custom_type("ThumbstickTurn", "AbstractControllerInput", preload("input/ThumbstickTurn.gd"), preload("icon.png"))
	add_custom_type("GrabHand", "RayCast3D", preload("input/GrabHand.gd"), preload("icon.png"))

func _exit_tree():
	remove_custom_type("BodyDirector")
	remove_custom_type("BodyRelocator")
	remove_custom_type("HandObserver")
	remove_custom_type("JumpButton")
	remove_custom_type("ThumbstickLocomotion")
	remove_custom_type("ThumbstickTurn")
	remove_custom_type("GrabHand")
