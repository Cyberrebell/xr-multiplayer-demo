extends Node

class_name AbstractControllerInput

@onready var controller : XRController3D = get_parent()
@onready var character_body : CharacterBody3D = BodyRelocator.find_character_body(controller)
var body_relocator : BodyRelocator

func init_relocator():
	body_relocator = BodyRelocator.find_body_relocator(character_body)
	body_relocator.character_body_updated.connect(update_body)

func update_body(body : CharacterBody3D) -> void:
	character_body = body
