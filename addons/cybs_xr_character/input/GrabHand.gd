extends ShapeCast3D

class_name GrabHand

@onready var controller : XRController3D = get_parent()
@onready var character_body : CharacterBody3D = BodyRelocator.find_character_body(controller)
var body_relocator : BodyRelocator

func init_relocator():
	body_relocator = BodyRelocator.find_body_relocator(character_body)
	body_relocator.character_body_updated.connect(update_body)

func update_body(body : CharacterBody3D) -> void:
	character_body = body


@export var max_pickup_mass : int = 80
@onready var xr_origin : XROrigin3D = BodyRelocator.find_xr_origin(controller)
@onready var joint : Generic6DOFJoint3D = Generic6DOFJoint3D.new()
var climb_connection
var grip
var hand_observer : HandObserver

signal climb_started
signal climb_stopped

func _ready() -> void:
	init_relocator()
	controller.button_pressed.connect(_on_controller_button_pressed)
	controller.button_released.connect(_on_controller_button_released)
	_setup_joint()

func grab() -> void:
	if is_colliding():
		var target = get_collider(0)
		if target is PhysicsBody3D:
			if target is RigidBody3D and target.mass < max_pickup_mass:
				joint.node_b = NodePath(target.get_path())
			elif XRGrip.has_grip(target):
				grip = XRGrip.get_grip(target)
			else:
				climb_started.emit()
				climb_connection = ClimbConnection.new(target, controller, character_body, xr_origin)
			controller.trigger_haptic_pulse("haptic", 60, 1, 0.1, 0)

func drop() -> void:
	if !joint.node_b.is_empty():
		joint.node_b = NodePath("")
	elif weakref(grip).get_ref() is XRGrip:
		grip = null
	elif climb_connection is ClimbConnection:
		climb_stopped.emit()
		body_relocator.climb_stopped.emit()
		climb_connection = null

func _on_controller_button_pressed(button_name : String) -> void:
	if button_name == "grip_click":
		grab()

func _on_controller_button_released(button_name : String) -> void:
	if button_name == "grip_click" and (!hand_observer is HandObserver or !hand_observer.is_hand_grabbing()):
		drop()

func _process(_delta):
	if climb_connection is ClimbConnection and climb_connection.is_active():
		character_body.velocity = Vector3.ZERO
		character_body.position = climb_connection.calculate_new_character_body_position()

func _physics_process(delta):
	if weakref(grip).get_ref() is XRGrip:
		grip.move_to(self)

func _setup_joint() -> void:
	var static_body = StaticBody3D.new()
	add_child(static_body)
	joint.set_param_x(Generic6DOFJoint3D.PARAM_ANGULAR_LIMIT_SOFTNESS, 0.01)
	joint.set_param_y(Generic6DOFJoint3D.PARAM_ANGULAR_LIMIT_SOFTNESS, 0.01)
	joint.set_param_z(Generic6DOFJoint3D.PARAM_ANGULAR_LIMIT_SOFTNESS, 0.01)
	static_body.add_child(joint)
	joint.node_a = NodePath(static_body.get_path())

class ClimbConnection:
	var target : PhysicsBody3D
	var controller : XRController3D
	var xr_origin : XROrigin3D
	var character_grab_position : Vector3
	var controller_grab_position : Vector3
	var target_grab_position : Vector3
	
	func _init(grab_target : PhysicsBody3D, grab_controller : XRController3D, character_body : CharacterBody3D, character_xr_origin : XROrigin3D):
		target = grab_target
		controller = grab_controller
		xr_origin = character_xr_origin
		character_grab_position = character_body.position
		controller_grab_position = controller.position
		target_grab_position = target.global_position

	func is_active() -> bool:
		return weakref(target).get_ref() is PhysicsBody3D

	func calculate_new_character_body_position() -> Vector3:
		return (target.global_position - target_grab_position) + character_grab_position + (controller_grab_position - controller.position).rotated(Vector3.UP, xr_origin.global_rotation.y)
