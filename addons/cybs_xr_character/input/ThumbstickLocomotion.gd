extends AbstractControllerInput

class_name ThumbstickLocomotion

const TOP_SPEED_MULTIPLIER : float = 2.0
const ACCELLERATION : float = 8.0

@export var movement_speed : float = 1.0

func _ready():
	init_relocator()

func _process(delta) -> void:
	var input_dir = controller.get_vector2("primary")
	input_dir.y = -input_dir.y
	if input_dir.length() > 0.8:
		input_dir = input_dir.normalized()
	elif input_dir.length() < 0.1:
		input_dir = Vector2.ZERO
	var direction = (character_body.transform.basis * Vector3(input_dir.x, 0, input_dir.y))
	character_body.velocity.x = move_toward(character_body.velocity.x, direction.x * movement_speed * TOP_SPEED_MULTIPLIER, delta * ACCELLERATION)
	character_body.velocity.z = move_toward(character_body.velocity.z, direction.z * movement_speed * TOP_SPEED_MULTIPLIER, delta * ACCELLERATION)
	character_body.move_and_slide()
