extends AbstractControllerInput

class_name JumpButton

const JUMP_POWER_MULTIPLIER : float = 5.0

@export var jump_button : String = "ax_button"
@export var jump_power : float = 1.0
var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")

func _ready():
	init_relocator()

func _physics_process(delta) -> void:
	if character_body.is_on_floor() or body_relocator.seat:
		if controller.get_input(jump_button):
			character_body.velocity.y = jump_power * JUMP_POWER_MULTIPLIER
			body_relocator.stand_up()
	else:
		character_body.velocity.y -= gravity * delta
