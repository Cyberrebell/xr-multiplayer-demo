extends AbstractControllerInput

class_name ThumbstickTurn

enum TurnMode
{
	SNAP,
	SMOOTH
}

@export var turn_mode : TurnMode = TurnMode.SNAP
@export var angle : int = 30
var snap_lock : bool = false
var processing_func : Callable

func _ready():
	if turn_mode == TurnMode.SMOOTH:
		processing_func = process_smooth_turn
	else:
		processing_func = process_snap_turn

func _physics_process(delta) -> void:
	processing_func.call(delta)

func process_smooth_turn(delta : float):
	var turn = get_turn_direction()
	if turn != 0:
		character_body.rotate_y(turn * delta)

func process_snap_turn(_delta : float):
	var turn = get_turn_direction()
	if snap_lock:
		if turn == 0:
			snap_lock = false
	else:
		if turn != 0:
			character_body.rotate_y(turn)
			snap_lock = true

func get_turn_direction() -> float:
	var input_dir = controller.get_vector2("primary")
	if input_dir.x > 0.8:
		return -deg_to_rad(angle)
	elif input_dir.x < -0.8:
		return deg_to_rad(angle)
	else:
		return 0
