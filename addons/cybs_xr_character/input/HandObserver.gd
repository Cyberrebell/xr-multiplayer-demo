extends Node

class_name HandObserver

@onready var hand : OpenXRHand = get_parent()
@onready var skeleton : Skeleton3D = hand.find_children("", "Skeleton3D").front()
@onready var grab_fingers : Array = _find_grab_tracking_bones()

func _ready():
	var controller : XRController3D = hand.get_parent().find_children("", "XRController3D").filter(_filter_matching_controller).front()
	controller.find_children("", "GrabHand").front().hand_observer = self

func is_hand_grabbing() -> bool:
	var fingers_grabbing = 0
	for finger in grab_fingers:
		if skeleton.get_bone_pose_rotation(finger).x < -0.7:
			fingers_grabbing += 1
	return fingers_grabbing > 1

func _find_grab_tracking_bones() -> Array:
	var bones = ["Index_Intermediate", "Middle_Intermediate", "Ring_Intermediate", "Little_Intermediate"]
	return bones.map(func(bone : String): return skeleton.find_bone(bone + _map_hand_to_bone_suffix()))

func _map_hand_to_bone_suffix() -> String:
	if hand.hand == OpenXRHand.HAND_LEFT:
		return "_L"
	else:
		return "_R"

func _filter_matching_controller(controller : XRController3D) -> bool:
	return controller.tracker == _map_hand_to_tracker_name()

func _map_hand_to_tracker_name() -> String:
	if hand.hand == OpenXRHand.HAND_LEFT:
		return "left_hand"
	else:
		return "right_hand"
