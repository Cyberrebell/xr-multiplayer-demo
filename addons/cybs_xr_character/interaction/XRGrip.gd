extends Node3D

class_name XRGrip

@onready var container : Node3D = get_parent()

func move_to(target : GrabHand):
	container.global_position = target.global_position
	container.global_rotation = target.global_rotation

static func has_grip(target : PhysicsBody3D) -> bool:
	return !target.find_children("", "XRGrip", false).is_empty()

static func get_grip(target : PhysicsBody3D) -> XRGrip:
	return target.find_children("", "XRGrip", false).front()
