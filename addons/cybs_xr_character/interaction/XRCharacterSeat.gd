extends Area3D

class_name XRCharacterSeat

@export var snap_radius : float = 0.3
@export var torso_height : float = 0.5
var head_target : CollisionShape3D

signal character_stand_up
signal character_sit_down

func _ready():
	head_target = CollisionShape3D.new()
	var shape = SphereShape3D.new()
	shape.radius = snap_radius
	head_target.shape = shape
	head_target.position.y = torso_height
	add_child(head_target)
	body_entered.connect(_on_body_entered)
	body_exited.connect(_on_body_exited)

func _on_body_entered(body):
	if body is CharacterBody3D:
		var body_relocator = body.find_children("", "BodyRelocator").front()
		if body_relocator is BodyRelocator:
			body_relocator.seat_in_range = self

func _on_body_exited(body):
	if body is CharacterBody3D:
		var body_relocator = body.find_children("", "BodyRelocator").front()
		if body_relocator is BodyRelocator:
			body_relocator.seat_in_range = null

func is_in_head_target(position : Vector3) -> bool:
	return head_target.global_position.distance_to(position) < snap_radius

func relocate(character : CharacterBody3D):
	character.global_transform = global_transform.translated_local(Vector3(0, -torso_height, 0))

func is_in_head_motion_range(pos : Vector3) -> bool:
	return Vector2(head_target.global_position.x, head_target.global_position.z).distance_to(Vector2(pos.x, pos.z)) < torso_height
