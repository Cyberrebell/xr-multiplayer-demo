extends XRGrip

class_name XRBoneGrip

@export var allow_rotation_x : bool = false
@export var allow_rotation_y : bool = false
@export var allow_rotation_z : bool = false

var bone_attachment : BoneAttachment3D
var skeleton : Skeleton3D
var bone_rotation : Vector3
var default_rotation : Vector3

signal rotated_x
signal rotated_y
signal rotated_z

func _ready():
	bone_attachment = container.get_parent()
	skeleton = bone_attachment.get_parent()
	bone_rotation = skeleton.get_bone_pose_rotation(bone_attachment.bone_idx).get_euler()
	default_rotation = get_current_rotation()

func move_to(target : GrabHand):
	var target_local = skeleton.to_local(target.global_position)
	var bone_local = skeleton.to_local(bone_attachment.global_position)
	if allow_rotation_x:
		var new_rot_x = (Vector2(bone_local.y, bone_local.z) - Vector2(target_local.y, target_local.z)).angle() - default_rotation.x
		rotated_x.emit(XRBoneGrip.cut_overflow_rotation(new_rot_x - bone_rotation.x))
		apply_haptic_feedback(target.controller, bone_rotation.x, new_rot_x)
		bone_rotation.x = new_rot_x
	if allow_rotation_y:
		var new_rot_y = -(Vector2(bone_local.x, bone_local.z) - Vector2(target_local.x, target_local.z)).angle() - default_rotation.y
		rotated_y.emit(XRBoneGrip.cut_overflow_rotation(new_rot_y - bone_rotation.y))
		apply_haptic_feedback(target.controller, bone_rotation.y, new_rot_y)
		bone_rotation.y = new_rot_y
	if allow_rotation_z:
		var new_rot_z = (Vector2(bone_local.x, bone_local.y) - Vector2(target_local.x, target_local.y)).angle() - default_rotation.z
		rotated_z.emit(XRBoneGrip.cut_overflow_rotation(new_rot_z - bone_rotation.z))
		apply_haptic_feedback(target.controller, bone_rotation.z, new_rot_z)
		bone_rotation.z = new_rot_z
	skeleton.set_bone_pose_rotation(bone_attachment.bone_idx, Quaternion.from_euler(bone_rotation))

func get_current_rotation() -> Vector3:
	var static_body : StaticBody3D = get_parent()
	return Vector3(
		Vector2(-static_body.position.x, -static_body.position.z).angle(),
		Vector2(static_body.position.y, static_body.position.z).angle(),
		Vector2(-static_body.position.x, -static_body.position.y).angle()
	)

static func apply_haptic_feedback(controller : XRController3D, old_rotation : float, new_rotation : float) -> void:
	if int(old_rotation * 6 / PI) - int(new_rotation * 6 / PI) != 0:
		controller.trigger_haptic_pulse("haptic", 60, 0.02, 0.01, 0)

static func cut_overflow_rotation(rot : float) -> float:
	if rot > PI:
		rot -= 2 * PI
	elif rot < -PI:
		rot += 2 * PI
	return rot
