extends MultiplayerSpawner

@onready var characters : Node3D = get_parent().find_child("characters")
var client_id : int

func _ready():
	spawn_function = spawn_player

func spawn_player(id : int) -> CharacterBody3D:
	var character_template : PackedScene
	if id == client_id:
		character_template = preload("res://character.tscn")
	else:
		character_template = preload("res://remote_character.tscn")
	var character = character_template.instantiate()
	character.name = str(id)
	character.position.x = randf_range(-5, 5)
	character.set_multiplayer_authority(id)
	return character

func despawn(id : int) -> void:
	characters.get_node(str(id)).queue_free()

@rpc
func set_client_id(id : int) -> void:
	client_id = id
