extends Node3D

@onready var spawner : CharacterSpawner = $CharacterSpawner

func _ready():
#	if "--client" in OS.get_cmdline_args():
#		init_client()
#	else:
#		init_server()
	if "--server" in OS.get_cmdline_args():
		init_server()
	else:
		init_client()

func init_server():
	print("launching server...")
	var peer = ENetMultiplayerPeer.new()
	peer.create_server(7023, 16)
	multiplayer.set_multiplayer_peer(peer)
	multiplayer.peer_connected.connect(_peer_connected)
	multiplayer.peer_disconnected.connect(_peer_disconnected)

func init_client():
	get_viewport().use_xr = true
	_client_connect()
	multiplayer.connected_to_server.connect(_connected_to_server)
	multiplayer.connection_failed.connect(_connection_failed)
	multiplayer.server_disconnected.connect(_server_disconnected)

func _client_connect():
	print("connecting to server...")
	var peer = ENetMultiplayerPeer.new()
	peer.create_client("127.0.0.1", 7023)
	multiplayer.set_multiplayer_peer(peer)

func _peer_connected(id):
	print("peer_connected " + str(id))
	spawner.multiplayer_spawner.rpc_id(id, "set_client_id", id)
	spawner.multiplayer_spawner.spawn(id)

func _peer_disconnected(id):
	print("peer_disconnected " + str(id))
	spawner.multiplayer_spawner.despawn(id)

func _connected_to_server():
	print("connected_to_server")

func _connection_failed():
	print("connection_failed")

func _server_disconnected():
	print("server_disconnected")
	_client_connect()
